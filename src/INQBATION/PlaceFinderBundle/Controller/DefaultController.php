<?php

namespace INQBATION\PlaceFinderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller {
    public function serializeCategories($categories) {
        $categoryText = '';
        foreach ($categories as $category) {
            if ($categoryText != '') {
                $categoryText .= ', ';
            }
            $categoryText .= $category['shortName'];
        }

        return $categoryText;
    }

    public function makeRequest($url) {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    public function findAction(Request $request) {
        $get = $request -> query -> all();

        $client_id = '4GJOZSWM5B4VR1EV21S4TXJFDVJN0M03YTBCPRUHZQBCYH2M';
        $client_secret = '0EIAH1ELXBQHNYM5TL31TVNUZODJ5AJASHD5KWENTBRM5KO5';
        $version = '20130815';
        $latitude = round(doubleval($get['latitude']), 2);
        $longitude = round(doubleval($get['longitude']), 2);
        $ll = "$latitude,$longitude";

        $response_array = null;
        if (isset($get['query'])) {
            $url = "https://api.foursquare.com/v2/venues/search?client_id=$client_id&client_secret=$client_secret&v=$version&ll=$ll&query={$get['query']}";

            $response = $this -> makeRequest($url);

            $temporary_array = json_decode($response, TRUE);
            $response_array = $temporary_array['response'];
            foreach ($response_array['venues'] as $key => $item) {
                $response_array['venues'][$key]['category_text'] = $this -> serializeCategories($item['categories']);
            }
        } else {
            $url = "https://api.foursquare.com/v2/venues/explore?client_id=$client_id&client_secret=$client_secret&v=$version&ll=$ll";

            $response = $this -> makeRequest($url);

            $temporary_array = json_decode($response, TRUE);
            foreach ($temporary_array['response']['groups'] as $item) {
                if ($item['type'] == 'Recommended Places') {
                    $response_array['venues'] = array();
                    foreach ($item['items'] as $key => $value) {
                        $value['venue']['category_text'] = $this -> serializeCategories($value['venue']['categories']);

                        $response_array['venues'][$key] = $value['venue'];
                    }
                    break;
                }
            }
        }

        return new Response(json_encode($response_array));
    }

    public function indexAction() {
        $array = array();
        $array['prefijo_url'] = $this -> get('router') -> generate('place_finder_homepage');

        return $this -> render('PlaceFinderBundle:Default:index.html.twig', $array);
    }

}
