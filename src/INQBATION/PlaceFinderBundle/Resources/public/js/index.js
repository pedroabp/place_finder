Ext.onReady(function() {
    var store = Ext.create('Ext.data.Store', {
        fields : ['id', 'name', 'location', {
            name : 'distance',
            convert : function(value, record) {
                return record.get('location').distance;
            }
        }, 'rating', 'category_text', 'categories', {
            name : 'category_images',
            convert : function(value, record) {
                var html = '';
                var categories = record.get('categories');
                for (var i = 0; i < categories.length; i++) {
                    html += '<img style="margin: 0px 2px 0px 0px;" src="' + categories[i].icon.prefix + 'bg_32' + categories[i].icon.suffix + '"></img>';
                };

                return html;
            }
        }],
        proxy : {
            type : 'ajax',
            url : 'find',
            reader : {
                type : 'json',
                root : 'venues'
            }
        }
    });

    var timerId = null;
    var marker = null;
    var grid = Ext.create('Ext.grid.Panel', {
        title : 'Place finder',
        store : store,
        region : 'center',
        height : 400,
        width : 800,
        tbar : [{
            xtype : 'radiofield',
            boxLabel : 'Search:',
            name : 'action',
            inputValue : 'm',
            id : 'search_radio',
            checked : true,
            listeners : {
                change : function(field, newValue) {
                    if (newValue) {
                        Ext.getCmp('search_textfield').setDisabled(false);
                        Ext.getCmp('search_textfield').focus();

                        store.removeAll();
                    }
                }
            }
        }, {
            id : 'search_textfield',
            xtype : 'textfield',
            // fieldLabel : 'Look for',
            labelWidth : 50,
            width : 300,
            listeners : {
                change : function(field, newValue) {
                    clearTimeout(timerId);

                    var callback = function() {

                        store.load({
                            params : {
                                latitude : Ext.getCmp('latitude_textfield').getValue(),
                                longitude : Ext.getCmp('longitude_textfield').getValue(),
                                query : newValue
                            }
                        });
                    };

                    timerId = setTimeout(callback, 500);
                }
            }
        }, '-', {
            xtype : 'radiofield',
            boxLabel : 'Explore',
            name : 'action',
            inputValue : 'l',
            id : 'explore_radio',
            listeners : {
                change : function(field, newValue) {
                    if (newValue) {
                        Ext.getCmp('search_textfield').setDisabled(true);

                        store.load({
                            params : {
                                latitude : Ext.getCmp('latitude_textfield').getValue(),
                                longitude : Ext.getCmp('longitude_textfield').getValue()
                            }
                        });
                    }
                }
            }
        }, '-', {
            id : 'latitude_textfield',
            xtype : 'textfield',
            fieldLabel : 'Latitude',
            hidden : true,
            labelWidth : 50
        }, {
            id : 'longitude_textfield',
            xtype : 'textfield',
            fieldLabel : 'Longitude',
            hidden : true,
            labelWidth : 50
        }],
        columns : [{
            text : 'Id',
            dataIndex : 'id',
            width : 100,
            hidden : true
        }, {
            text : 'Name',
            dataIndex : 'name',
            flex : 1
        }, {
            text : 'Address',
            dataIndex : 'location',
            flex : 1,
            renderer : function(value) {
                return value.address;
            }
        }, {
            text : 'Distance (meters)',
            dataIndex : 'distance',
            flex : 1
        }, {
            text : 'Rating',
            dataIndex : 'rating',
            align : 'right',
            width : 50
        }, {
            text : 'Categories',
            dataIndex : 'category_images',
            flex : 1
        }],
        listeners : {
            select : function(model, record) {
                var latitude = record.get('location').lat;
                var longitude = record.get('location').lng;

                var pos = new google.maps.LatLng(latitude, longitude);

                map.gmap.panTo(pos);

                var options = {
                    map : map.gmap,
                    position : pos
                };

                if (marker != null) {
                    marker.setMap(null);
                }

                marker = new google.maps.Marker(options);
            }
        }
    });

    var map = Ext.create('Ext.ux.GMapPanel', {
        xtype : 'gmappanel',
        region : 'south',
        height : '50%',
        mapOptions : {
            mapTypeId : google.maps.MapTypeId.ROADMAP
        },
        center : {
            lat : 3.3757533053302,
            lng : -76.5322523145
        }
    });

    Ext.create('Ext.container.Viewport', {
        layout : 'border',
        items : [grid, map]
    });

    function showPosition(position) {
        Ext.getCmp('latitude_textfield').setValue(position.coords.latitude);

        Ext.getCmp('longitude_textfield').setValue(position.coords.longitude);
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        Ext.Msg.alert('Information', 'Geolocation is not supported by this browser.');
    }
});

